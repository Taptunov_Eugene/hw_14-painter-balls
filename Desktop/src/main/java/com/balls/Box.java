package com.balls;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Box {

    double minX, maxX, minY, maxY;  // Box's bounds (package access)
    private Color colorFilled;   // Box's filled color (background)
    private static final Color DEFAULT_COLOR_FILLED = Color.BLACK;

    /**
     * Constructors
     */
    public Box(double x, double y, double width, double height, Color colorFilled) {
        minX = x;
        minY = y;
        maxX = x + width - 1;
        maxY = y + height - 1;
        this.colorFilled = colorFilled;
    }

    /**
     * Constructor with the default color
     */
    public Box(double x, double y, double width, double height) {

        this(x, y, width, height, DEFAULT_COLOR_FILLED);
    }

    /**
     * Set or reset the boundaries of the box.
     */
    public void set(double x, double y, double width, double height) {
        minX = x;
        minY = y;
        maxX = x + width - 1;
        maxY = y + height - 1;
    }

    /**
     * Draw itself using the given graphic context.
     */
    public void draw(GraphicsContext context) {
        context.setFill(colorFilled);
        context.fillRect(minX, minY, maxX - minX - 1, maxY - minY - 1);
    }
}
