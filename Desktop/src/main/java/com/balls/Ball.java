package com.balls;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Ball {

    float x, y; // Ball's center x and y (package access)
    float radius; // Ball's radius (package access)
    float speedX, speedY; // Ball's speed per step in x and y (package access)
    Color color; // Ball's color
    int width;
    int height;
    private final static Color DEFAULT_COLOR = Color.BLACK;

    /**
     * Constructor: For user friendliness, user specifies velocity in speed and
     * moveAngle in usual Cartesian coordinates. Need to convert to speedX and
     * speedY in Java graphics coordinates for ease of operation.
     */
    public Ball(float x, float y, float radius, float speed, float angle, Color color) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        // Convert (speed, angle) to (x, y), with y-axis inverted
        this.speedX = (float) (speed * Math.cos(Math.toRadians(angle)));
        this.speedY = -speed * (float) Math.sin(Math.toRadians(angle));
    }

    /**
     * Constructor with the default color
     */
    public Ball(float x, float y, float radius, float speed, float angle) {
        this(x, y, radius, speed, angle, DEFAULT_COLOR);
    }

    public void updatePositionWithCollisionDetection(Box box) {
        // Get the ball's bounds, offset by the radius of the ball
        float ballMinX = (float) box.minX + radius;
        float ballMinY = (float) box.minY + radius;
        float ballMaxX = (float) box.maxX - radius;
        float ballMaxY = (float) box.maxY - radius;

        // Calculate the ball's new position
        x += speedX;
        y += speedY;
        // Check if the ball moves over the bounds. If so, adjust the position and speed.
        if (x < ballMinX) {
            speedX = -speedX; // Reflect along normal
            x = ballMinX;     // Re-position the ball at the edge
        } else if (x > ballMaxX) {
            speedX = -speedX;
            x = ballMaxX;
        }
        // May cross both x and y bounds
        if (y < ballMinY) {
            speedY = -speedY;
            y = ballMinY;
        } else if (y > ballMaxY) {
            speedY = -speedY;
            y = ballMaxY;
        }
    }

    /**
     * Draw itself using the given graphics context.
     */
    public void draw(GraphicsContext context) {
        context.setFill(color);
        context.fillOval((int) (x - radius), (int) (y - radius), (int) (2 * radius), (int) (2 * radius));
    }

    /**
     * Return the magnitude of speed.
     */
    public float getSpeed() {
        return (float) Math.sqrt(speedX * speedX + speedY * speedY);
    }

    /**
     * Return the direction of movement in degrees (counter-clockwise).
     */
    public float getMoveAngle() {
        return (float) Math.toDegrees(Math.atan2(-speedY, speedX));
    }

    /**
     * Return mass
     */
    public float getMass() {
        return radius * radius * radius / 1000f;  // Normalize by a factor
    }

    /**
     * Return the kinetic energy (0.5mv^2)
     */
    public float getKineticEnergy() {
        return 0.5f * getMass() * (speedX * speedX + speedY * speedY);
    }
}
